# Docker LAMP Setup 

This repo provides a very basic Docker setup with nginx reverse proxy.


## Install reverse proxy

To be able to run several different projects with this setup on one host
machine, we need to establish a http reverse proxy. For this we can
use the jwilder/nginx-proxy docker image like this:

**docker-compose.yml**
```
version: "3"

networks:
  default:
    external:
      name: http-proxy

services:
  http-proxy:
    image: jwilder/nginx-proxy
    ports:
      - "80:80"
      - "443:443"
    volumes:
      - /var/run/docker.sock:/tmp/docker.sock:ro
    restart: always

```

On first ``docker-compose up`` we get an error message which tells us
to create a network named "http-proxy". You can do this with this command:
``docker network create http-proxy``

Once the network is created and the container is established you don't 
need to execute this steps again.

## Install new project

Just clone this repo and rename the value of the environment variable
**VIRTUAL_HOST** in docker-compose.yml file. Perform ``docker-compose up`` 
and enjoy.

When the containers are running you can access the http services
with the desired VIRTUAL_HOST name.


### Hosts

You need to modify your local hosts file, so the given host names (e.g. "project.local")
are pointing to localhost.

```
127.0.0.1 project.local another-project.local
```

If your OS supports wildcards in hosts file (Windows doesn't), you can define
hosts dynamically, like this:

```
127.0.0.1 *.local
```
